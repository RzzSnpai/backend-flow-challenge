# Backend-Flow-Challenge

This Project use NodeJS and Fastify.

### How to run

- Clone this repo and install all dependencies

```sh
npm install
```

- Create a .env file in root directory
```sh
BASE_URL_WEATHER="https://weatherapi-com.p.rapidapi.com"
API_KEY_WEATHER="API_SECRET_KEY"
API_HOST_WEATHER="weatherapi-com.p.rapidapi.com"
PORT_API=5000
```
- Run the proyect
```sh
npm start
```

Open the proyect in your localhost port `5000`

### Routes

| Method | URL                   | Response                    |
| ------ | --------------------- | ----------------------------|
| get    | `/v1/location`        | Return client location      |
| get    | `/v1/current/`        | Return current weather      |
| get    | `/v1/forecast/`       | Return next forecast weather|

### Testing

In this project i used [Jest](https://jestjs.io/) for testing, you can run this tests with the following command

Run unit test

```sh
npm test
```

- For watch coverage use

```sh
npm test -- --coverage --collectCoverageFrom="./src/**"
```

### Swagger

- To see the swagger go to /docs

```sh
http://localhost:5000/docs
```


- Eduardo Barrios