import utils from '../src/utils/formatter';

describe('user local', () => {
  test('without ip', () => {
    expect(utils('::1')).toBe('auto:ip');
  });
  test('with ip', () => {
    expect(utils('127.0.0.1')).toBe('auto:ip');
  });
});

describe('normal user', () => {
  test('normal ip', () => {
    expect(utils('190.29.84.8')).toBe('190.29.84.8');
  });
});
