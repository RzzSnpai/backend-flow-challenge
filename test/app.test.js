import build from '../src/app';
jest.useFakeTimers()
import fetch from 'node-fetch';

describe('endpoints calls', () => {
    test ('example', () => {
        const app = build()
        app.listen({port:0},async (err) =>{
            await fetch(`http://localhost:${fastify.server.address().port}/v1/location`)
            .then((response) => response.json())
            .then((response) => {
                expect(response).toEqual({test: "test"})
            })
            .catch((err) =>{
                expect(err).toEqual({error: 'test'})
            })

        })
    
    })
    test ('async example',async () => {
        const app = build()
      
        app.inject({
          method: 'GET',
          url: '/v1/current/',
          query: {
            city: ['Cordoba,brasil']
          },
          payload: {
              statusCode: 200
          }
        })
        .then(res => {
          expect(res.json()).toEqual({test: "test"})
      
        })
        .catch(err => expect(err).toEqual({error: 'test'}))
    })
})