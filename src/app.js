import fastify from 'fastify';
import dotenv from 'dotenv';
import weatherRoutes from './routes/weather';

dotenv.config();

function build(opts = {}) {
  const app = fastify(opts);
  app.register(require('@fastify/swagger'), {
    mode: 'dynamic',
    exposeRoute: true,
    routePrefix: '/docs',
    swagger: {
      info: {
        title: 'Backend-Flow-Challenge API',
        description: 'REST API with Node.js and Fastify',
        version: '1.0.0',
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here',
      },
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
  });
  app.register(weatherRoutes, { prefix: '/v1' });
  return app;
}

export default build;
