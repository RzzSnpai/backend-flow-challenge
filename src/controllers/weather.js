import * as fetch from 'node-fetch';
import * as dotenv from 'dotenv';
import formatterIP from '../utils/formatter';
import dataParse from '../utils/forecastFormatter';

dotenv.config();
const options = {
  method: 'GET',
  headers: {
    'X-RapidAPI-Key': process.env.API_KEY_WEATHER,
    'X-RapidAPI-Host': process.env.API_HOST_WEATHER,
  },
};

export const getCurrentController = async (req, reply) => {
  const result = [];
  let isError = false;
  let { city } = req.query;
  if (city) {
    city = city[0].split(',');
    const promise = city.map(async (value) => {
      try {
        const data = await fetch(`${process.env.BASE_URL_WEATHER}/current.json?q=${value}`, options);
        const response = await dataParse(data);
        result.push(response);
      } catch (error) {
        isError = error;
      }
    });
    await Promise.all(promise);
    if (isError) reply.send(isError);
    else reply.send(result);
  } else {
    try {
      const data = await fetch(`${process.env.BASE_URL_WEATHER}/current.json?q=${formatterIP(req.socket.remoteAddress)}`, options);
      const response = await dataParse(data);
      result.push(response);
      reply.send(result);
    } catch (error) {
      reply.send(error);
    }
  }
};

export const getForecastController = async (req, reply) => {
  const result = [];
  let isError = false;
  const { city } = req.query;
  if (city) {
    const cities = city[0].split(',');
    const promises = cities.map(async (value) => {
      try {
        const data = await fetch(`${process.env.BASE_URL_WEATHER}/forecast.json?q=${value}&days=5`, options);
        const response = await dataParse(data);
        result.push(response);
      } catch (error) {
        isError = error;
      }
    });
    await Promise.all(promises);
    if (isError) reply.send(isError);
    else reply.send(result);
  } else {
    try {
      const data = await fetch(`${process.env.BASE_URL_WEATHER}/forecast.json?q=${formatterIP(req.socket.remoteAddress)}&days=5`, options);
      const response = await dataParse(data);
      result.push(response);
      reply.send(result);
    } catch (error) {
      reply.send(error);
    }
  }
};
export const getLocationController = async (req, reply) => {
  try {
    const data = await fetch(`${process.env.BASE_URL_WEATHER}/ip.json?q=${formatterIP(req.socket.remoteAddress)}`, options);
    const dataJson = await data.json();
    reply.send(dataJson);
  } catch (error) {
    reply.send(error);
  }
};
