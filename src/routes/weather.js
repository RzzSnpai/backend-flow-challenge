import getLocationSchema from '../schemas/getLocation';
import getCurrentSchema from '../schemas/getCurrent';
import getForecastSchema from '../schemas/getForecast';
import { getLocationController, getCurrentController, getForecastController } from '../controllers/weather';

export default function weather(fastify, _, done) {
  fastify.get('/current/', getCurrentSchema, getCurrentController);
  fastify.get('/forecast/', getForecastSchema, getForecastController);
  fastify.get('/location', getLocationSchema, getLocationController);
  done();
}
