const dataParse = async (data) => {
  const dataJson = await data.json();
  const response = {
    location: dataJson.location,
    current: dataJson.current.condition.text,
    forecast: dataJson.forecast,
  };
  return response;
};

export default dataParse;
