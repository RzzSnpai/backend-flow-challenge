const formatterIP = (ip) => {
  switch (ip) {
    case '::1':
      return 'auto:ip';
    case '127.0.0.1':
      return 'auto:ip';
    default:
      return ip;
  }
};

export default formatterIP;
