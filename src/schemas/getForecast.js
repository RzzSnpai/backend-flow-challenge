import conditionText from './utils/conditionText';
import currentLocation from './utils/getCurrentLocation';
import error from './utils/error';
import querystring from './utils/cityQueryString';

export default {
  schema: {
    querystring,
    tags: ['Weather'],
    description: 'city information, current weather and forecast for the next 5 days',
    response: {
      200: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            location: currentLocation,
            current: {
              type: 'string',
            },
            forecast: {
              type: 'object',
              properties: {
                forecastday: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: {
                      date: { type: 'string' },
                      day: {
                        type: 'object',
                        properties: {
                          condition: conditionText,
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
      400: error,
    },
  },
};
