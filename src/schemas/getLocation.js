import error from './utils/error';
import location from './utils/getLocationOK';

export default {
  schema: {
    tags: ['Weather'],
    description: 'information about the city client',
    summary: 'get client information',
    response: {
      200: location,
      400: error,
    },
  },
};
