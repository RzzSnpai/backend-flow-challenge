import error from './utils/error';
import querystring from './utils/cityQueryString';
import currentLocation from './utils/getCurrentLocation';

export default {
  schema: {
    querystring,
    tags: ['Weather'],
    description: 'city information and current weather',
    response: {
      200: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            location: currentLocation,
            current: { type: 'string' },
          },
        },
      },
      400: error,
    },
  },
};
