export default {
  type: 'object',
  properties: {
    ip: { type: 'string' },
    type: { type: 'string' },
    continent_code: { type: 'string' },
    country_name: { type: 'string' },
    region: { type: 'string' },
    city: { type: 'string' },
    tz_id: { type: 'string' },
  },
};
