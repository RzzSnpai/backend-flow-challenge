export default {
  type: 'object',
  properties: {
    name: { type: 'string' },
    region: { type: 'string' },
    country: { type: 'string' },
    tz_id: { type: 'string' },
  },
};
