export default {
  city: {
    type: 'array',
    items: {
      type: 'string',
    },
    minItems: 0,
    maxItems: 5,
  },
};
