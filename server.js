const server = require('./src/app').default({
  logger: {
    level: 'info',
    transport: {
      target: 'pino-pretty',
    },
  },
});

server.listen({ port: process.env.PORT_API }, (err, address) => {
  if (err) {
    server.log.error(err);
    process.exit(1);
  }
});
